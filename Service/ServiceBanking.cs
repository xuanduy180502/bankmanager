﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Service
{
    public class ServiceBanking
    {
        private Customer[] customers;
        private Customer[] orders;
        int top;
        int bot = -1;
        int amount = 0;
        string filepath = @"C:\Users\duy\Desktop\C#\BankingMenu\bank.txt";
        string fileShow = @"C:\Users\duy\Desktop\C#\BankingMenu\show.txt";
        int index = 0;
        public ServiceBanking()
        {
            orders = new Customer[5];
            customers = new Customer[5];
            top = 0;
        }
        public void Input(Customer customer)
        {
            Console.Write("Name: ");
            customer.Name = Console.ReadLine();
            Console.Write("Amount: ");
            customer.Amount = int.Parse(Console.ReadLine());
            customer = new Customer
            {
                Amount = customer.Amount,
                Name = customer.Name
            };
        }

        public void AddCustomerToQueue(Customer customer)
        {
            bot++;
            customer.STT = bot + 1;
            if (customer.Amount >= 10)
            {
                for (int i = bot; i > top; i--)
                {
                    customers[i] = customers[i - 1];

                    if (customer.Amount < customers[top].Amount)
                    {

                        customers[top] = customer;
                    }

                }
                
                customers[top] = customer;

            }

            else
            {
                customers[bot] = customer;
            }
        }
        public Customer CallNextCustomer()
        {
            var customer = customers[top];
            bot++;
            if (customer != null)
            {
                for (int i = top; i <= bot; i++)
                {
                    customers[i] = customers[i + 1];
                }
                bot--;
            }
            amount += customer.Amount;
            return customer;
        }
        public void WriteFile()
        {
            var thongke = Thongke();
            var show = Show();
            if (!File.Exists(filepath) || !File.Exists(fileShow))
            {
                File.Create(filepath);
                File.Create(fileShow);
            }
            else
            {
                File.WriteAllText(filepath, thongke);
                File.WriteAllText(fileShow, show);
            }
        }
        public string Thongke()
        {
            int remain = 0;
            foreach (var customer in customers)
            {
                if (customer != null)
                {
                    remain += customer.Amount;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.Append($"\"Total amount withdrawn: {amount} \nShow total amount remaining: {remain}");
            Console.WriteLine($"\"Total amount withdrawn: {amount} \nShow total amount remaining: {remain}");
            return sb.ToString();
        }

        public string Show()
        {
            StringBuilder sb = new StringBuilder();
            //for (int i = bot; i >= bot - 3; i--)
            //{
            //    if (customers[i] != null)
            //    {
            //        Console.WriteLine("Số: | " + customers[i].STT + " - " + customers[i].Name + " - " + customers[i].Amount + " | ");
            //        sb.Append($"Số: |  {customers[i].STT}  -   {customers[i].Name}   -   {customers[i].Amount} | \n");
            //    }
            //}
            for (int i = top; i <=bot; i++)
            {
                if (customers[i] != null)
                {
                    Console.WriteLine("Số: | " + customers[i].STT + " - " + customers[i].Name + " - " + customers[i].Amount + " | ");
                    sb.Append($"Số: |  {customers[i].STT}  -   {customers[i].Name}   -   {customers[i].Amount} | \n");
                }
            }
            return sb.ToString();
        }
    }

}

