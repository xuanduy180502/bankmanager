﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingMenu
{
    public class Menu
    {
        public void BankMenu()
        {
            Console.WriteLine("----------Bank Management---------");
            Console.WriteLine("| 1. Add Customer on wait list.  |");
            Console.WriteLine("| 2. Call the next customer.     |");
            Console.WriteLine("| 3. View the customer.          |");
            Console.WriteLine("| 4. Statistic table.            |");
            Console.WriteLine("| 5. Exit.                       |");
            Console.WriteLine("----------------------------------");
        }

    }
}
