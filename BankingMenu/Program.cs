﻿using BankingMenu;
using Service;
Console.OutputEncoding = System.Text.Encoding.UTF8;


var service = new ServiceBanking();
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.BankMenu();
        Console.WriteLine("choice");

        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Customer customer = new Customer();
                service.Input(customer);

                service.AddCustomerToQueue(customer);
                break;
            case 2:
                var nextCustomer = service.CallNextCustomer();

                Console.WriteLine("Customer: " + nextCustomer.Name + " |" + " Money Withdraw: " + nextCustomer.Amount);
                break;
            case 3:
                service.Show();
                break;
            case 4:
                service.WriteFile();
                break;
            default:
                Console.WriteLine("Good Bye!");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }

} while (choice < 5);




